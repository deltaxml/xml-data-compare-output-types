# Output Types

The result file that is output can be changed.  The default is to have a deltaV2 format file that shows all content with both changed and unchanged data.

You can choose to have just the changes shown so that any elements where A=B are excluded from the result.

Another option is to have a side-by-side folding diff report, which can be downloaded and displayed in a browser.

## REST request:

Replace {LOCATION} below with your location of the download. 



```
<compare>
    <inputA xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="file">
        <path>{LOCATION}/addressesA.xml</path>
    </inputA>
    <inputB xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="file">
        <path>{LOCATION}/addressesB.xml</path>
    </inputB>
    <configuration xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="file">
        <path>{LOCATION}/config-sbs-folding-diffreport.xml</path>
    </configuration>
</compare>
```


See the [XML Data Compare REST User Guide](https://docs.deltaxml.com/xdc/latest/rest-user-guide) for how to run the comparison using REST.
